# Inheritance
extends Node

# Public variables
onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var ranking_system = get_node(@"/root/GameRoot").ranking_system
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# Private variables
onready var _texture_frame_node = get_node(@"TextureFrame")
onready var _button_back_node = get_node(@"TextureFrame/Button_Back")
onready var _label_rankings_node = get_node(@"TextureFrame/Label_Rankings")

# Engine methods
func _ready():
	_button_back_node.connect("pressed", self, "_on_back_pressed")
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		_texture_frame_node.set_theme(load("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", false)
	else: # Classic Mode
		_texture_frame_node.set_theme(load("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", true)
	# *****************************************************************
	
	# Load and show rankings
	ranking_system.open("user://rankings.dat")
	
	var rankings = ranking_system.get_first_n_rankings(10)
	
	var rankings_text = ""
	
	for ranking in rankings:
		rankings_text += ranking.player_name + " " + ranking.record + "\n"
		
	_label_rankings_node.set_text(rankings_text)

# Public methods
func end():
	current_wkf_element.end()

# Private methods
func _on_back_pressed():
	current_wkf_element.set("next_element_name", "MainMenu")
	call_deferred("end")