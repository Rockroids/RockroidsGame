# Inheritance
extends Node

# Enumerations
enum {ACTION_NEWGAME = 0, ACTION_RANKINGS = 1, ACTION_OPTIONS = 2, ACTION_CREDITS = 3, ACTION_QUIT = 4}

# Public variables
onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# Private variables
onready var _texture_frame_node = get_node(@"TextureFrame")
onready var _texture_frame_logo_node = get_node(@"TextureFrame/TextureFrame_Logo")
onready var _button_newgame_node = get_node(@"TextureFrame/CenterContainer/GridContainer/Button_NewGame")
onready var _button_rankings_node = get_node(@"TextureFrame/CenterContainer/GridContainer/Button_Rankings")
onready var _button_options_node = get_node(@"TextureFrame/CenterContainer/GridContainer/Button_Options")
onready var _button_credits_node = get_node(@"TextureFrame/CenterContainer/GridContainer/Button_Credits")
onready var _button_quit_node = get_node(@"TextureFrame/CenterContainer/GridContainer/Button_Quit")
onready var _stream_player_node = get_node(@"StreamPlayer")

# Engine methods
func _ready():
	_button_newgame_node.connect("pressed", self, "_on_option_pressed", [0])
	_button_rankings_node.connect("pressed", self, "_on_option_pressed", [1])
	_button_options_node.connect("pressed", self, "_on_option_pressed", [2])
	_button_credits_node.connect("pressed", self, "_on_option_pressed", [3])
	_button_quit_node.connect("pressed", self, "_on_option_pressed", [4])
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		_texture_frame_node.set_theme(preload("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_texture_frame_logo_node.set_texture(preload("res://Assets/GFX/Default/UI/UI_Logo.png"))
		_stream_player_node.set_stream(preload("res://Assets/Music/Default/MainTheme.ogg"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", false)
	else: # Classic Mode
		_texture_frame_node.set_theme(preload("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_texture_frame_logo_node.set_texture(preload("res://Assets/GFX/Classic/UI/UI_Logo.png"))
		_stream_player_node.set_stream(preload("res://Assets/Music/Classic/MainTheme.ogg"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", true)
	# *****************************************************************

	_stream_player_node.play()

# Public methods
func end():
	current_wkf_element.end()

# Private methods
func _on_option_pressed(option):
	if option == ACTION_NEWGAME:
		current_wkf_element.set("next_element_name", "Ingame")
	elif option == ACTION_RANKINGS:
		current_wkf_element.set("next_element_name", "Rankings")
	elif option == ACTION_OPTIONS:
		current_wkf_element.set("next_element_name", "Options")
	elif option == ACTION_CREDITS:
		current_wkf_element.set("next_element_name", "Credits")
	else:
		current_wkf_element.set("next_element_name", "")
		
	call_deferred("end")
