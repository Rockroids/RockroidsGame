# Inheritance
extends Node

# Script instances
const SpaceShip = preload("res://Scripts/Components/SpaceShip.gd")
const Asteroid = preload("res://Scripts/Components/Asteroid.gd")
const EnemyShip = preload("res://Scripts/Components/EnemyShip.gd")
const EnemyMotherShip = preload("res://Scripts/Components/EnemyMotherShip.gd")
const SceneResourcePooler = preload("res://Scripts/Core/SceneResourcePooler.gd")

# Enumerations
enum {TYPE_ASTEROID = 0, TYPE_ENEMYSHIP = 1, TYPE_MOTHERSHIP = 2}
enum {STATE_IDLE = -1, STATE_ROUND_ACTIVE = 0, STATE_CHANING_ROUND = 1, STATE_GAME_OVER = 2}

# Public variables
export(int) var min_asteroid_fragments = 0
export(int) var max_asteroid_fragments = 4
export(int) var max_motherships_spawn = 4
export(int) var max_asteroids_on_screen = 5
export(int) var max_enemy_ships_on_screen = 2
export(int) var max_mother_ships_on_screen = 1
export(Texture) var weapon_type1_level1_texture = null
export(Texture) var weapon_type1_level2_texture = null
export(Texture) var weapon_type2_level1_texture = null
export(Texture) var weapon_type2_level2_texture = null
export(Texture) var weapon_type3_level1_texture = null
export(Texture) var weapon_type3_level2_texture = null
export(float) var spawn_time = 1.0
export(float) var check_time = 2.0
export(float) var message_time = 5.0

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var ranking_system = get_node(@"/root/GameRoot").ranking_system
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

var projectiles_pool = null
var asteroids_pool = null
var enemy_ships_pool = null
var enemy_motherships_pool = null
var powerups_pool = null

var state = STATE_IDLE
var score = 0
var round_number = 1
var asteroids_remaining = 0
var enemy_ships_remaining = 0
var mother_ships_remaining = 0

# Private variables
onready var _ship_node = get_node(@"CanvasLayer_Game/Locals_Layer/SpaceShip")
onready var _label_lifes_value_node = get_node(@"CanvasLayer_HUD/HBoxContainer/Label_Lifes_Value")
onready var _label_score_value_node = get_node(@"CanvasLayer_HUD/HBoxContainer/Label_Score_Value")
onready var _textureprogress_life_node = get_node(@"CanvasLayer_HUD/HBoxContainer/TextureProgress_Life")
onready var _textureprogress_shield_node = get_node(@"CanvasLayer_HUD/HBoxContainer/TextureProgress_Shield")
onready var _label_weapon_missile_remaining_node = get_node(@"CanvasLayer_HUD/HBoxContainer/Label_Weapon_Missile_Remaining")
onready var _label_weapon_laser_remaining_node = get_node(@"CanvasLayer_HUD/HBoxContainer/Label_Weapon_Laser_Remaining")
onready var _texturerect_weapon_common_level_node = get_node(@"CanvasLayer_HUD/HBoxContainer/TextureRect_Weapon_Common_Level")
onready var _texturerect_weapon_missile_level_node = get_node(@"CanvasLayer_HUD/HBoxContainer/TextureRect_Weapon_Missile_Level")
onready var _texturerect_weapon_laser_level_node = get_node(@"CanvasLayer_HUD/HBoxContainer/TextureRect_Weapon_Laser_Level")
onready var _pickupeables_layer_node = get_node(@"CanvasLayer_Game/Pickupeables_Layer")
onready var _projectiles_layer_node = get_node(@"CanvasLayer_Game/Projectiles_Layer")
onready var _elements_layer_node = get_node(@"CanvasLayer_Game/Elements_Layer")
onready var _locals_layer_node = get_node(@"CanvasLayer_Game/Locals_Layer")
onready var _timer_spawn_node = get_node(@"Timer_Spawn")
onready var _timer_check_node = get_node(@"Timer_Check")
onready var _timer_message_node = get_node(@"Timer_Message")
onready var _label_message_node = get_node(@"CanvasLayer_HUD/Label_Message")
onready var _panel_pausemenu_node = get_node(@"CanvasLayer_PauseMenu/Panel_PauseMenu")
onready var _button_pausemenu_continue_node = get_node(@"CanvasLayer_PauseMenu/Panel_PauseMenu/CenterContainer/VBoxContainer/Button_PauseMenu_Continue")
onready var _button_pausemenu_exittomainmenu_node = get_node(@"CanvasLayer_PauseMenu/Panel_PauseMenu/CenterContainer/VBoxContainer/Button_PauseMenu_ExitToMainMenu")
onready var _stream_player_node = get_node(@"StreamPlayer")
onready var _texture_frame_node = get_node("CanvasLayer_BG/TextureFrame")
onready var _hbox_container_node = get_node("CanvasLayer_HUD/HBoxContainer")

# Engine methods
func _ready():
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		_texture_frame_node.set_theme(preload("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", false)
		_hbox_container_node.set_theme(preload("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_label_message_node.set_theme(preload("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_panel_pausemenu_node.set_theme(preload("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		
		_textureprogress_life_node.set_over_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Over.png"))
		_textureprogress_life_node.set_under_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Under.png"))
		_textureprogress_life_node.set_progress_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Progress_Life.png"))
		
		_textureprogress_shield_node.set_over_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Over.png"))
		_textureprogress_shield_node.set_under_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Under.png"))
		_textureprogress_shield_node.set_progress_texture(preload("res://Assets/GFX/Default/UI/UI_ProgressBar_Progress_Shield.png"))
		
		weapon_type1_level1_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type1_Level1.png")
		weapon_type1_level2_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type1_Level2.png")
		weapon_type2_level1_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type2_Level1.png")
		weapon_type2_level2_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type2_Level2.png")
		weapon_type3_level1_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type3_Level1.png")
		weapon_type3_level2_texture = preload("res://Assets/GFX/Default/UI/UI_Icon_Weapon_Type3_Level2.png")
		
		_stream_player_node.set_stream(preload("res://Assets/Music/Default/Battle_1.ogg"))
	else: # Classic Mode
		_texture_frame_node.set_theme(preload("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", true)
		_hbox_container_node.set_theme(preload("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_label_message_node.set_theme(preload("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_panel_pausemenu_node.set_theme(preload("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		
		_textureprogress_life_node.set_over_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Over.png"))
		_textureprogress_life_node.set_under_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Under.png"))
		_textureprogress_life_node.set_progress_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Progress_Life.png"))
		
		_textureprogress_shield_node.set_over_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Over.png"))
		_textureprogress_shield_node.set_under_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Under.png"))
		_textureprogress_shield_node.set_progress_texture(preload("res://Assets/GFX/Classic/UI/UI_ProgressBar_Progress_Shield.png"))
		
		weapon_type1_level1_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type1_Level1.png")
		weapon_type1_level2_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type1_Level2.png")
		weapon_type2_level1_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type2_Level1.png")
		weapon_type2_level2_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type2_Level2.png")
		weapon_type3_level1_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type3_Level1.png")
		weapon_type3_level2_texture = preload("res://Assets/GFX/Classic/UI/UI_Icon_Weapon_Type3_Level2.png")
		
		_stream_player_node.set_stream(preload("res://Assets/Music/Classic/Battle_1.ogg"))
	# *****************************************************************
	
	apply_parameters()
	
	# Load pools
	projectiles_pool = SceneResourcePooler.new("res://Assets/Components/CommonProjectile.tscn", _projectiles_layer_node.get_path(), 50)
	asteroids_pool = SceneResourcePooler.new("res://Assets/Components/Asteroid.tscn", _elements_layer_node.get_path(), 30, false)
	enemy_ships_pool = SceneResourcePooler.new("res://Assets/Components/EnemyShip.tscn", _elements_layer_node.get_path(), 30, false)
	enemy_motherships_pool = SceneResourcePooler.new("res://Assets/Components/EnemyMotherShip.tscn", _elements_layer_node.get_path(), 10, false)
	powerups_pool = SceneResourcePooler.new("res://Assets/Components/PowerUp.tscn", _elements_layer_node.get_path(), 20, false)
	
	_timer_check_node.start()
	_timer_spawn_node.start()
	
	_stream_player_node.play()

	set_process(true)

func _process(delta):
	if Engine.get_main_loop().is_paused():
		return

	if Input.is_action_just_pressed("pause"):
		_panel_pausemenu_node.show()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		Engine.get_main_loop().set_pause(true)
		return

	_label_lifes_value_node.set_text(str(_ship_node.lifes))
	_label_score_value_node.set_text(str(score))
	_textureprogress_life_node.set_value(int(_ship_node.life))
	_textureprogress_shield_node.set_value(int(_ship_node.shield))
	_label_weapon_missile_remaining_node.set_text(str(_ship_node.weapon_missile_remaining))
	_label_weapon_laser_remaining_node.set_text(str(_ship_node.weapon_laser_remaining))
	
	match _ship_node.weapon_common_level:
		1:
			_texturerect_weapon_common_level_node.set_texture(weapon_type1_level1_texture)
		2:
			_texturerect_weapon_common_level_node.set_texture(weapon_type1_level2_texture)
		
	match _ship_node.weapon_missile_level:
		1:
			_texturerect_weapon_missile_level_node.set_texture(weapon_type2_level1_texture)
		2:
			_texturerect_weapon_missile_level_node.set_texture(weapon_type2_level2_texture)
	
	match _ship_node.weapon_laser_level:
		1:
			_texturerect_weapon_laser_level_node.set_texture(weapon_type3_level1_texture)
		2:
			_texturerect_weapon_laser_level_node.set_texture(weapon_type3_level2_texture)

# Public methods
func get_ship_node():
	return _ship_node

func spawn_powerup(position, type, value):
	var powerup = powerups_pool.get_new_resource()
	
	powerup.set_global_position(position)
	powerup.type = type
	powerup.value = value
	
	powerup.run()

func spawn_enemy(type):
	var position = Vector2(0.0,0.0)
	var impulse = Vector2(0.0,0.0)
	
	# Set random impulse
	impulse = Vector2(rand_range(-1.0,1.0), rand_range(-1.0,1.0))
	
	# Set spawn position
	var zone = randi() % 4 + 1
	var zone_rect = Rect2(0.0,0.0,0.0,0.0)
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	
	match zone:
		1:
			zone_rect = Rect2(-real_viewport_size.x * 0.1, -real_viewport_size.y * 0.1, real_viewport_size.x + (real_viewport_size.x * 0.1 * 2), real_viewport_size.y * 0.1)
		2:
			zone_rect = Rect2(-real_viewport_size.x * 0.1, 0, real_viewport_size.x * 0.1, real_viewport_size.y)
		3:
			zone_rect = Rect2(real_viewport_size.x, 0, real_viewport_size.x * 0.1, real_viewport_size.y)
		4:
			zone_rect = Rect2(-real_viewport_size.x * 0.1, real_viewport_size.y, real_viewport_size.x + (real_viewport_size.x * 0.1 * 2), real_viewport_size.y * 0.1)
		
	position = Vector2(rand_range(zone_rect.position.x, zone_rect.end.x), rand_range(zone_rect.position.y, zone_rect.end.y))
	
	# Spawn element
	match type:
		TYPE_ASTEROID:
			var asteroid = asteroids_pool.get_new_resource()
			asteroid.set_global_position(position)
			asteroid.impulse = impulse
			asteroid.type = randi() % 3 + 1
			asteroid.subtype = 0
			asteroid.fragmentable = true if randi() % 2 > 0 else false
			asteroid.fragments = 0 if not asteroid.fragmentable or max_asteroid_fragments == 0 else randi() % max_asteroid_fragments + min_asteroid_fragments
			
			asteroid.run()
			asteroids_remaining -= 1
		TYPE_ENEMYSHIP:
			var enemy_ship = enemy_ships_pool.get_new_resource()
			
			enemy_ship.set_global_position(position)
			enemy_ship.impulse = impulse
			
			enemy_ship.run()
			
			enemy_ships_remaining -= 1
		TYPE_MOTHERSHIP:
			var mother_ship = enemy_motherships_pool.get_new_resource()
			
			mother_ship.max_spawned_ships = max_motherships_spawn
			mother_ship.set_global_position(position)
			mother_ship.impulse = impulse
			
			mother_ship.run()
			
			mother_ships_remaining -= 1

func get_screen_elements_number(type):
	var elements = {TYPE_ASTEROID: 0, TYPE_ENEMYSHIP: 0, TYPE_MOTHERSHIP: 0}
	
	elements[TYPE_ASTEROID] = asteroids_pool.get_used_resources_number()
	elements[TYPE_ENEMYSHIP] = enemy_ships_pool.get_used_resources_number()
	elements[TYPE_MOTHERSHIP] = enemy_motherships_pool.get_used_resources_number()
	
	return elements[type]

func apply_parameters():
	_timer_spawn_node.wait_time = spawn_time
	_timer_spawn_node.one_shot = false
	_timer_spawn_node.connect("timeout", self, "_on_spawn_timeout")
	
	_timer_check_node.wait_time = check_time
	_timer_check_node.one_shot = false
	_timer_check_node.connect("timeout", self, "_on_check_timeout")
	
	_timer_message_node.wait_time = message_time
	_timer_message_node.one_shot = true
	_timer_message_node.connect("timeout", self, "_on_message_timeout")
	
	_panel_pausemenu_node.hide()
	_button_pausemenu_continue_node.connect("pressed", self, "_on_continue_pressed")
	_button_pausemenu_exittomainmenu_node.connect("pressed", self, "_on_exittomainmenu_pressed")
	
	_label_message_node.hide()

func change_round():
	round_number += 1
	state = STATE_CHANING_ROUND
	
	# Show the round	
	_label_message_node.set_text(TranslationServer.translate("Ingame.Round") + " " + str(round_number))
	_label_message_node.show()
	
	# Set number of enemies according to round
	var difficulty = preferences_system.get_parameter("Game", "Difficult", 1)
	var difficulty_factor = 0.0
	
	match difficulty:
		0:
			difficulty_factor = 0.5
		1:
			difficulty_factor = 1.0
		2:
			difficulty_factor = 2.0

	asteroids_remaining = int(((round_number / 2) + 2) * difficulty_factor)
	enemy_ships_remaining = int(((0.6 * round_number) - 0.8) * difficulty_factor)
	mother_ships_remaining = int((0.1 * round_number) * difficulty_factor)
	max_motherships_spawn = int(clamp(((0.3 * round_number) - 2) * difficulty_factor, 0.0, 4.0))
	min_asteroid_fragments = int(clamp(((0.33 * round_number) - 0.33) * difficulty_factor, 0.0, 4.0))
	max_asteroid_fragments = int(clamp(((0.25 * round_number) + 1) * difficulty_factor, 0.0, 4.0))
	 
	_timer_message_node.start()

func end():
	current_wkf_element.end()

# Private methods
func _on_continue_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Engine.get_main_loop().set_pause(false)
	_panel_pausemenu_node.hide()

func _on_exittomainmenu_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Engine.get_main_loop().set_pause(false)
	_panel_pausemenu_node.hide()
	current_wkf_element.set("next_element_name", "MainMenu")
	call_deferred("end")

func _on_spawn_timeout():
	if state != STATE_ROUND_ACTIVE:
		return
	
	# Spawn enemy
	if asteroids_remaining > 0 and get_screen_elements_number(TYPE_ASTEROID) < max_asteroids_on_screen:
		spawn_enemy(TYPE_ASTEROID)
		return
	
	if enemy_ships_remaining > 0 and get_screen_elements_number(TYPE_ENEMYSHIP) < max_enemy_ships_on_screen:
		spawn_enemy(TYPE_ENEMYSHIP)
		return
	
	if mother_ships_remaining > 0 and get_screen_elements_number(TYPE_MOTHERSHIP) < max_mother_ships_on_screen:
		spawn_enemy(TYPE_MOTHERSHIP)
		return
	
func _on_check_timeout():
	if asteroids_remaining == 0 and enemy_ships_remaining == 0 and mother_ships_remaining == 0 and get_screen_elements_number(TYPE_ASTEROID) == 0 and get_screen_elements_number(TYPE_ENEMYSHIP) == 0 and get_screen_elements_number(TYPE_MOTHERSHIP) == 0 and _ship_node.state == SpaceShip.STATE_ALIVE:
		change_round()
		
	if _ship_node.state == SpaceShip.STATE_DIED and state != STATE_GAME_OVER:
		state = STATE_GAME_OVER
		_label_message_node.set_text(TranslationServer.translate("Ingame.GameOver"))
		_label_message_node.show()
		_timer_message_node.start()

func _on_message_timeout():
	if state == STATE_CHANING_ROUND:
		state = STATE_ROUND_ACTIVE
		_label_message_node.hide()
	
	if state == STATE_GAME_OVER:
		# Add ranking
		var date = OS.get_datetime()
		var date_string = str(date.year) + "-" + str(date.month) + "-" + str(date.day) + " " + str(date.hour) + ":" + str(date.minute) + ":" + str(date.second)
		
		ranking_system.open("user://rankings.dat")
		ranking_system.add_new_ranking(date_string, preferences_system.get_parameter("Game", "PlayerName", "Player"), score)
		ranking_system.save("user://rankings.dat")
		
		# Go to the Main Menu
		current_wkf_element.set("next_element_name", "MainMenu")
		call_deferred("end")
