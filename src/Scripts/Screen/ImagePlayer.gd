# Inheritance
extends Node

# Enumerations
enum {STATE_IDLE = -1, STATE_FADEIN = 0, STATE_SHOWING = 1, STATE_FADEOUT = 2}

# Public variables
export(String, FILE, "*.png") var image_name = ""
export(String, FILE, "*.ogg") var audio_name = ""
export(bool) var finish_when_audio_ends = true
export(float) var show_time = 2.0
export(float) var fade_time = 0.0
export(Color) var fade_color = Color(0.0,0.0,0.0)

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element

# Private variables
onready var _texture_frame_node = get_node(@"TextureFrame")
onready var _stream_player_node = get_node(@"StreamPlayer")
onready var _timer_node = get_node(@"Timer")
onready var _timer_fade_node = get_node(@"Timer_Fade")

var _state = STATE_IDLE

# Engine methods
func _ready():
	set_process_input(true)
	set_process(true)
	
	# Import parameters from workflow	
	if current_wkf_element:
		if current_wkf_element.parameters.has("ImageName"):
			image_name = current_wkf_element.parameters["ImageName"]
		
		if current_wkf_element.parameters.has("AudioName"):
			audio_name = current_wkf_element.parameters["AudioName"]
			
		if current_wkf_element.parameters.has("FinishWhenAudioEnds"):
			finish_when_audio_ends = current_wkf_element.parameters["FinishWhenAudioEnds"]
		
		if current_wkf_element.parameters.has("ShowTime"):
			show_time = current_wkf_element.parameters["ShowTime"]
		
		if current_wkf_element.parameters.has("FadeTime"):
			fade_time = current_wkf_element.parameters["FadeTime"]
		
		if current_wkf_element.parameters.has("FadeColor"):
			fade_color = current_wkf_element.parameters["FadeColor"]

	_timer_node.set_wait_time(show_time)
	_timer_node.one_shot = true
	_timer_fade_node.set_wait_time(fade_time)
	_timer_fade_node.one_shot = true
	_timer_fade_node.connect("timeout", self, "_on_fade_finished")
	
	# Load the image
	if image_name:
		_texture_frame_node.set_texture(load(image_name))
	
	# Set the timer parameters and start
	if not finish_when_audio_ends:
		_timer_node.connect("timeout", self, "_on_finished")
	
	# Set the stream player parameters and start
	if audio_name:
		_stream_player_node.set_stream(load(audio_name))
		if finish_when_audio_ends:
			_stream_player_node.connect("finished", self, "_on_finished")
	
	_texture_frame_node.get_material().set_shader_param("mix_color", fade_color)
	
	_state = STATE_FADEIN
	_timer_fade_node.start()

func _input(event):
	if event.is_action_pressed("cancel"):
		set_process_input(false)
		call_deferred("end")

func _process(delta):
	var mix_factor = 0.0
	
	if _state == STATE_FADEIN:
		mix_factor = 1.0 - ((_timer_fade_node.get_wait_time() - _timer_fade_node.get_time_left()) / _timer_fade_node.get_wait_time())
	
	if _state == STATE_FADEOUT:
		mix_factor = ((_timer_fade_node.get_wait_time() - _timer_fade_node.get_time_left()) / _timer_fade_node.get_wait_time())
	
	# Set shader parameter that affects mixing
	_texture_frame_node.get_material().set_shader_param("mix_factor", mix_factor)

# Public methods
func end():
	current_wkf_element.end()

# Private methods
func _on_finished():
	_state = STATE_FADEOUT
	_timer_fade_node.start()

func _on_fade_finished():
	if _state == STATE_FADEIN:
		_state = STATE_SHOWING
		if not finish_when_audio_ends:
			_timer_node.start()
		else:
			_stream_player_node.play()
	else: # Fadeout state
		call_deferred("end")
