# Inheritance
extends Node

# Public variables
onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# Private variables
onready var _texture_frame_node = get_node(@"TextureFrame")
onready var _label_title_node = get_node(@"TextureFrame/Label_Title")
onready var _label_credits_node = get_node(@"TextureFrame/Label_Credits")
onready var _button_back_node = get_node(@"TextureFrame/Button_Back")

# Engine methods
func _ready():
	_button_back_node.connect("pressed", self, "_on_back_pressed")	
	_label_credits_node.set_text(TranslationServer.translate("Credits.Programmer") + " - Alexia Rodriguez\n" + TranslationServer.translate("Credits.Designer") + " - Adrian Bertini")
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		_texture_frame_node.set_theme(load("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", false)
	else: # Classic Mode
		_texture_frame_node.set_theme(load("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		_texture_frame_node.get_material().set_shader_param("classic_mode", true)
	# *****************************************************************

# Public methods
func end():
	current_wkf_element.end()

# Private methods
func _on_back_pressed():
	current_wkf_element.set("next_element_name", "MainMenu")
	call_deferred("end")
