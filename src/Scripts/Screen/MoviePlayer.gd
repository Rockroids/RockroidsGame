# Inheritance
extends Node

# Public variables
export(String, FILE, "*.ogv") var movie_name = ""

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element

# Private variables
onready var _video_player_node = get_node(@"VideoPlayer")

# Engine methods
func _ready():
	set_process(true)
	set_process_input(true)
	
	# Import parameters from workflow	
	if current_wkf_element:
		if current_wkf_element.parameters.has("MovieName"):
			movie_name = current_wkf_element.parameters["MovieName"]

	if movie_name and video_player_node:
		_video_player_node.set_stream(load(movie_name))
		_video_player_node.play()

func _input(event):
	if event.is_action_pressed("cancel"):
		set_process_input(false)
		_on_finished()

func _process(delta):
	if not _video_player_node.is_playing():
		set_process(false)
		_on_finished()

# Private methods
func _on_finished():
	_video_player_node.stop()
	current_wkf_element.end()