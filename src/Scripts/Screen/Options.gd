# Inheritance
extends Node

# Enumerations
enum {ACTION_CANCEL = 0, ACTION_SAVE = 1}

# Variables
var actions = []
var current_action_name = null

# Public variables
onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# Engine methods
func _ready():
	set_process_input(true)
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		get_node(@"TextureFrame").set_theme(load("res://Assets/GFX/Default/Themes/UI_Theme.tres"))
		get_node(@"TextureFrame").get_material().set_shader_param("classic_mode", false)
	else: # Classic Mode
		get_node(@"TextureFrame").set_theme(load("res://Assets/GFX/Classic/Themes/UI_Theme.tres"))
		get_node(@"TextureFrame").get_material().set_shader_param("classic_mode", true)
	# *****************************************************************
	
	# Map button signals
	get_node(@"TextureFrame/Button_Cancel").connect("pressed", self, "_on_button_pressed", [ACTION_CANCEL])
	get_node(@"TextureFrame/Button_Save").connect("pressed", self, "_on_button_pressed", [ACTION_SAVE])
	
	get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").connect("item_selected", self, "_on_control_type_selected")
	
	# Initialize actions array
	for action in preferences_system.get_available_actions():
		actions.append({"action": action, "name": preferences_system.get_action_name(action), "input_event": {0: InputEvent.new(), 1: InputEvent.new()}, "button": null})
	
	# Load preferences
	preferences_system.load_preferences()
	
	var index = 0
	
	for language in preferences_system.get_available_languages():
		get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Language").add_item(language["name"], index)
		index += 1

	index = 0
	for resolution in preferences_system.get_available_resolutions():
		get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Resolution").add_item(str(resolution["resolution"].x) + " x " + str(resolution["resolution"].y) + " (" + resolution["name"] + ")", index)
		index += 1
		
	index = 0
	for difficult in preferences_system.get_available_difficuties():
		get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Difficult").add_item(difficult, index)
		index += 1
	
	get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").add_item(TranslationServer.translate("ControlType.Keyboard"), preferences_system.CONTROL_TYPE_KEYBOARD)
	get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").add_item(TranslationServer.translate("ControlType.Joystick"), preferences_system.CONTROL_TYPE_JOYSTICK)
	
	# Add control map elements
	for action in actions:
		var action_label = Label.new()
		var action_button = Button.new()
		
		action.button = action_button
		
		action_label.set_text(action.name)
		action_button.set_text("Undefined")
		
		action_button.connect("pressed", self, "_on_control_button_pressed", [action.action])
		
		get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer").add_child(action_label)
		get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer").add_child(action_button)
		
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Resolution").select(preferences_system.get_parameter("Game", "Resolution", 0))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/CheckButton_Fullscreen").set_pressed(preferences_system.get_parameter("Game", "Fullscreen", true))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/LineEdit_PlayerName").set_text(preferences_system.get_parameter("Game", "PlayerName", "Player"))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/HSlider_SFXVolume").set_value(preferences_system.get_parameter("Game", "SFXVolume", 0.0))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/HSlider_MusicVolume").set_value(preferences_system.get_parameter("Game", "MusicVolume", 0.0))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Difficult").select(preferences_system.get_parameter("Game", "Difficult", 1))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/CheckButton_ClassicalMode").set_pressed(preferences_system.get_parameter("Game", "ClassicalMode", false))
	get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Language").select(preferences_system.get_parameter("Game", "Language", 0))
	
	# Load actions
	get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").select(preferences_system.get_parameter("Controls", "ControlType", 0))

	for action in preferences_system.get_available_actions():
		get_action(action).input_event[preferences_system.CONTROL_TYPE_KEYBOARD] = preferences_system.get_parameter("Controls_Keyboard&Mouse", action, preferences_system.get_action_default_input_event(action, preferences_system.CONTROL_TYPE_KEYBOARD))
	
	for action in preferences_system.get_available_actions():
		get_action(action).input_event[preferences_system.CONTROL_TYPE_JOYSTICK] = preferences_system.get_parameter("Controls_Joystick", action, preferences_system.get_action_default_input_event(action, preferences_system.CONTROL_TYPE_JOYSTICK))

	refresh_control_buttons()

func _input(event):	
	if current_action_name and get_node(@"TextureFrame/PopupDialog").is_visible():
		if (get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").get_selected_id() == preferences_system.CONTROL_TYPE_KEYBOARD and event is InputEventKey) or (get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").get_selected_id() == preferences_system.CONTROL_TYPE_JOYSTICK and (event is InputEventJoypadButton or event is InputEventJoypadMotion)):
			# Fix the joystick axis values
			if event is InputEventJoypadMotion:
				event.axis_value = -1.0 if event.axis_value < 0 else 1.0

			if not has_action_input_event(event):
				get_action(current_action_name).input_event[get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").get_selected_id()] = event
				current_action_name = null
				get_node(@"TextureFrame/PopupDialog").hide()
				refresh_control_buttons()

# Public methods
func refresh_control_buttons():
	for action in actions:
		get_action(action.action).button.set_text(get_input_event_string(get_action(action.action).input_event[get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").get_selected_id()]))

func get_action(action_name):
	for action in actions:
		if action.action == action_name:
			return action
	
	return null

func get_input_event_string(event):
	if event is InputEventKey:
		return OS.get_scancode_string(event.scancode)
	elif event is InputEventJoypadButton:
		return "Button " + str(event.button_index)
	elif event is InputEventJoypadMotion:
		return "Axis " + str(event.axis) + " " + ("Left" if event.axis_value < 0 else "Right")
	else:
		return "Undefined"

func has_action_input_event(event):	
	for action in actions:
		var event_to_compare = action.input_event[get_node(@"TextureFrame/TabContainer_Options/Controls/CenterContainer/GridContainer/OptionButton_ControlType").get_selected_id()]
		
		if not event_to_compare.get_class() == event.get_class():
			continue
		
		if  event is InputEventKey:
			if event.scancode == event_to_compare.scancode:
				return true
		elif event is InputEventJoypadButton:
			if event.button_index == event_to_compare.button_index:
				return true
		elif event == InputEventJoypadMotion:
			if event.axis == event_to_compare.axis and event.axis_value == event_to_compare.axis_value:
				return true

	return false

func end():
	current_wkf_element.end()

# Private methods
func _on_control_type_selected(id):
	refresh_control_buttons()

func _on_button_pressed(action):
	if action == ACTION_SAVE:
		# Save preferences
		preferences_system.set_parameter("Game", "Resolution", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Resolution").get_selected_id())
		preferences_system.set_parameter("Game", "Fullscreen", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/CheckButton_Fullscreen").is_pressed())
		preferences_system.set_parameter("Game", "PlayerName", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/LineEdit_PlayerName").get_text())
		preferences_system.set_parameter("Game", "SFXVolume", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/HSlider_SFXVolume").get_value())
		preferences_system.set_parameter("Game", "MusicVolume", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/HSlider_MusicVolume").get_value())
		preferences_system.set_parameter("Game", "Difficult", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Difficult").get_selected_id())
		preferences_system.set_parameter("Game", "ClassicalMode", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/CheckButton_ClassicalMode").is_pressed())
		preferences_system.set_parameter("Game", "Language", get_node(@"TextureFrame/TabContainer_Options/Game Options/CenterContainer/GridContainer/OptionButton_Language").get_selected_id())
		
		# Save actions
		for action in preferences_system.get_available_actions():
			preferences_system.set_parameter("Controls_Keyboard&Mouse", action, get_action(action).input_event[preferences_system.CONTROL_TYPE_KEYBOARD])
			preferences_system.set_parameter("Controls_Joystick", action, get_action(action).input_event[preferences_system.CONTROL_TYPE_JOYSTICK])
		
		preferences_system.save_preferences()
		preferences_system.apply_preferences()
	
	current_wkf_element.set("next_element_name", "MainMenu")
	
	call_deferred("end")

func _on_control_button_pressed(action_name):
	current_action_name = action_name
	get_node(@"TextureFrame/PopupDialog").popup()

