# Inheritance
extends Node

# Enumerations
enum {WORKFLOW_ELEMENT_PERMAMENT = 0, WORKFLOW_ELEMENT_DYNAMIC = 1}

# Public variables
export(String, FILE) var cursor_texture_path = ""
export(NodePath) var workflow_permanent_elements_path = @""
export(NodePath) var workflow_dynamic_elements_path = @""

onready var root_node = get_node(@"/root/GameRoot")

# Engine methods
func _ready():
	set_process(true)
	
	root_node.logging_system.init(root_node.logging_system.LOG_TYPE_CONSOLE)
	root_node.preferences_system.init("user://preferences.cfg")

	root_node.preferences_system.load_preferences()
	root_node.preferences_system.apply_preferences()
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if root_node.preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		cursor_texture_path = "res://Assets/GFX/Default/UI/UI_Cursor.png"
		print("Default mode")
	else: # Classic Mode
		print("Classic Mode")
		cursor_texture_path = "res://Assets/GFX/Classic/UI/UI_Cursor.png"
	# *****************************************************************
	
	root_node.workflow_system.cursor_texture_path = cursor_texture_path
	root_node.workflow_system.dynamic_elements_nodepath = workflow_dynamic_elements_path
	root_node.workflow_system.permanent_elements_nodepath = workflow_permanent_elements_path

	root_node.workflow_system.add_element_simple("IntroScreen", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/ImagePlayer.tscn", false, "CompanyScreen")
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if root_node.preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		root_node.workflow_system.get_element_by_name("IntroScreen").parameters["ImageName"] = "res://Assets/GFX/Default/UI/UI_IntroScreen.png"
	else: # Classic mode
		root_node.workflow_system.get_element_by_name("IntroScreen").parameters["ImageName"] = "res://Assets/GFX/Classic/UI/UI_IntroScreen.png"
	# *****************************************************************
	
	root_node.workflow_system.get_element_by_name("IntroScreen").parameters["ShowTime"] = 3.0
	root_node.workflow_system.get_element_by_name("IntroScreen").parameters["FadeTime"] = 2.0
	root_node.workflow_system.get_element_by_name("IntroScreen").parameters["AudioName"] = ""
	root_node.workflow_system.get_element_by_name("IntroScreen").parameters["FinishWhenAudioEnds"] = false

	root_node.workflow_system.root_element = root_node.workflow_system.get_element_by_name("IntroScreen")

	root_node.workflow_system.add_element_simple("CompanyScreen", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/ImagePlayer.tscn", false, "MainMenu")
	
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if root_node.preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["ImageName"] = "res://Assets/GFX/Default/UI/UI_CompanyLogo.png"
	else: # Classic mode
		root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["ImageName"] = "res://Assets/GFX/Classic/UI/UI_CompanyLogo.png"
	# *****************************************************************
	
	root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["ShowTime"] = 3.0
	root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["FadeTime"] = 2.0
	root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["AudioName"] = ""
	root_node.workflow_system.get_element_by_name("CompanyScreen").parameters["FinishWhenAudioEnds"] = false

	root_node.workflow_system.add_element_simple("MainMenu", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/MainMenu.tscn", true)

	root_node.workflow_system.add_element_simple("Options", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/Options.tscn", true, "", true)
	
	root_node.workflow_system.add_element_simple("Rankings", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/Rankings.tscn", true, "", true)

	root_node.workflow_system.add_element_simple("LoadingScreen", WORKFLOW_ELEMENT_PERMAMENT, "res://Assets/Screens/LoadingScreen.tscn", false)
	
	root_node.workflow_system.add_element_simple("Credits", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/Credits.tscn", true, "", true)
	
	root_node.workflow_system.add_element_simple("Ingame", WORKFLOW_ELEMENT_DYNAMIC, "res://Assets/Screens/Ingame.tscn", false, "", false)
	
	root_node.logging_system.log_message(root_node.logging_system.LOG_MESSAGE_DEBUG, "Initalize done.")
	
	# Start the workflow system
	root_node.workflow_system.start()

func _process(delta):
	root_node.workflow_system.process_iteration()
