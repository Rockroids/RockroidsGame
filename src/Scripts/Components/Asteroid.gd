# Inheritance
extends RigidBody2D

# Script instances
const SpaceShip = preload("res://Scripts/Components/SpaceShip.gd")

# Public variables
export(int,1,3) var type = 1
export(int,0,3) var subtype = 0
export(bool) var fragmentable = false
export(int,0,4) var fragments = 0
export(float) var fragment_radius = 100.0
export(Vector2) var impulse = Vector2(0.0,0.0)
export(float) var life = 10.0
export(float) var speed = 200.0
export(float) var damage = 5.0
export(int) var score = 20

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# PoolableResource variables
var is_pool_initialized = false
var is_pool_added_to_tree_on_init = false

# Private variables
onready var _timer_node = get_node(@"Timer")
onready var _explosion_particles_node = get_node(@"Explosion_Particles")
onready var _sprite_node = get_node(@"Sprite")
onready var _explosion_audio_player_node = get_node(@"Explosion_AudioPlayer")
var _collision_nodes = []
var _ingame_node = null
var _actual_life = 10.0

var _explosion_particles_texture = null

# Signals
signal died

# Engine methods
func _notification(what):
	if what == NOTIFICATION_INSTANCED:
		# *****************************************************************
		# Classic / Default mode
		# *****************************************************************
		if Engine.get_main_loop().get_root().get_node("/root/GameRoot").preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Default/Sprites/Asteroid/Asteroid_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Asteroid_Explosion.ogg"))
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))
			_explosion_particles_texture = preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png")
		else: # Classic Mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Classic/Sprites/Asteroid/Asteroid_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Asteroid_Explosion.ogg"))
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
			_explosion_particles_texture = preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png")
		# *****************************************************************
		
		get_node(@"Timer").connect("timeout", self, "_on_timer_node_timeout")
		connect("body_entered", self, "_on_body_entered")
		
		for n_type in range(1,4):
			for n_subtype in range(0,4):
				_collision_nodes.append(get_node("Collision_" + get_type_string(n_type) + "_" + get_subtype_string(n_subtype)))

func _ready():
	_ingame_node = get_node(str(current_wkf_element.scene_node.get_path()) + "/Ingame")
	
	reset()

func _process(delta):
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()

	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)

# Public methods
func apply_parameters():
	# Set sprite_node
	_sprite_node.set_animation(get_type_string(type) + get_subtype_string(subtype))
	
	if subtype > 0:
		life = 1.0

	# Set collisions
	for n_type in range(1,4):
		for n_subtype in range(0,4):
			if type == n_type and subtype == n_subtype:
				_collision_nodes[((4 * n_type) - 4) + (n_subtype)].set_disabled(false)
			else:
				_collision_nodes[((4 * n_type) - 4) + (n_subtype)].set_disabled(true)

func get_type_string(type):
	return "Type" + str(type)

func get_subtype_string(subtype):
	if subtype == 0:
		return "Parent"
	else:
		return "Child" + str(subtype)

func apply_damage(value):
	_actual_life = 0.0 if _actual_life <= value else _actual_life - value
	
	if _actual_life == 0.0:
		emit_signal("died")
		die()

func die():
	_timer_node.start()
	_explosion_audio_player_node.play()
	#set_mode(RigidBody2D.MODE_STATIC)
	set_linear_velocity(Vector2(0.0,0.0))
	
	_explosion_particles_node.set_texture(_explosion_particles_texture)
	_explosion_particles_node.emitting = true
	
	# Hide sprite_node
	_sprite_node.set_visible(false)
	
	# Disable collisions
	for collision_node in _collision_nodes:
		collision_node.set_disabled(true)
	
	# Add score
	_ingame_node.score += score
	
	# Spawn powerup
	if subtype == 3:
		_ingame_node.spawn_powerup(get_global_position(), randi() % 5 + 1, randi() % 20 + 10)
	 
	# Fragment in parts
	if fragmentable and fragments > 0:
		var n_fragments = [] # {'origin': Vector2(), 'impulse': Vector2()}
		
		var begin_angle_deg = rand_range(0,360)
		var current_angle = 0.0
		
		for n_fragment in range(1,fragments + 1):
			current_angle = deg2rad(begin_angle_deg + ((360 / fragments) * (n_fragment - 1)))
			
			var origin = Vector2(fragment_radius * -cos(current_angle), fragment_radius * -sin(current_angle)) + get_global_position()
			var impulse = Vector2(-cos(current_angle), -sin(current_angle))
			
			n_fragments.append({'origin' : origin, 'impulse': impulse})
			
		
		for n_fragment in n_fragments:
			var asteroid = _ingame_node.asteroids_pool.get_new_resource()
			
			asteroid.set_global_position(n_fragment.origin)
			asteroid.fragmentable = false
			asteroid.fragments = 0
			asteroid.type = type
			asteroid.subtype = randi() % 3 + 1
			asteroid.impulse = n_fragment.impulse

			asteroid.run()

func reset():
	set_process(false)
	set_physics_process(false)
	set_contact_monitor(false)
	set_linear_velocity(Vector2(0.0,0.0))
	set_collision_mask(0)
	set_collision_layer(0)
	set_sleeping(true)
	
	_actual_life = life
	_sprite_node.set_visible(false)
	_explosion_particles_node.set_texture(null)
	_explosion_particles_node.emitting = false
	_explosion_audio_player_node.stop()
	_timer_node.stop()
	
	# Disable collisions
	for collision_node in _collision_nodes:
		collision_node.set_disabled(true)
			

func run():
	var time = OS.get_ticks_msec()
	set_contact_monitor(true)
	set_sleeping(false)
	set_collision_mask_bit(0, true)
	set_collision_layer_bit(0, true)
	set_process(true)
	set_physics_process(true)
	
	print("TIME: " + str(OS.get_ticks_msec() - time))
	
	apply_parameters()
	
	_sprite_node.set_visible(true)
	
	set_linear_velocity(impulse.normalized() * speed)
	
	print("TIME1: " + str(OS.get_ticks_msec() - time))

# Private methods
func _on_timer_node_timeout():
	self.pool_free()

func _on_body_entered(body):
	if body.has_method("apply_damage"):
		body.apply_damage(damage)
	pass

# PoolableResource methods
func pool_initialize():
	is_pool_initialized = true

func pool_free():
	reset()
	
	if not is_pool_added_to_tree_on_init:
		get_parent().remove_child(self)

	is_pool_initialized = false
