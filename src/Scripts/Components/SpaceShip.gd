# Inheritance
extends RigidBody2D

# Script instances
const PowerUp = preload("res://Scripts/Components/PowerUp.gd")
const CommonProjectileObj = preload("res://Assets/Components/CommonProjectile.tscn")

# Enumerations
enum {STATE_ALIVE = 1, STATE_DYING = 2, STATE_DIED = 3}

# Public variables
export(float) var shooting_time_interval = 0.5
export(int, 1, 3) var state = 1
export(bool) var invulnerable = false
export(float) var invulnerability_time = 3.0
export(float) var explosion_time = 3.0
export(float) var speed = 250.0
export(float) var rotation_speed = 10000.0
export(int) var lifes = 3
export(float) var life = 100.0
export(float) var shield = 100.0
export(int,1,2) var weapon_common_level = 1
export(int,1,2) var weapon_missile_level = 1
export(int,1,2) var weapon_laser_level = 1
export(int) var weapon_missile_remaining = 0
export(int) var weapon_laser_remaining = 0

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# Private variables
onready var _sprite_node = get_node(@"Sprite")
onready var _particles_node = get_node(@"Particles")
onready var _collision_node = get_node(@"Collision")
onready var _projectile_position_node = get_node(@"Projectile_Position")
onready var _explosion_particles_node = get_node(@"Explosion_Particles")
onready var _timer_invulnerability_node = get_node(@"Timer_Invulnerability")
onready var _timer_explosion_node = get_node(@"Timer_Explosion")
onready var _explosion_audioplayer_node = get_node(@"Explosion_AudioPlayer")

var _shoot_time_accumulator = 0.0
var _ingame_node = null

# Signals
signal died

# Engine methods
func _ready():
	# *****************************************************************
	# Classic / Default mode
	# *****************************************************************
	if preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
		_sprite_node.set_sprite_frames(preload("res://Assets/GFX/Default/Sprites/SpaceShip/SpaceShip_Sprites.tres"))
		_explosion_audioplayer_node.set_stream(preload("res://Assets/SFX/Default/Explosion.ogg"))
		_particles_node.set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))
		_explosion_particles_node.set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))
	else: # Classic Mode
		_sprite_node.set_sprite_frames(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/SpaceShip_Sprites.tres"))
		_explosion_audioplayer_node.set_stream(preload("res://Assets/SFX/Classic/Explosion.ogg"))
		_particles_node.set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
		_explosion_particles_node.set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
		
	# *****************************************************************

	_ingame_node = get_node(str(current_wkf_element.scene_node.get_path()) + "/Ingame")
	
	apply_preferences()
	
	set_process(true)
	
	reset()

func _process(delta):
	if _shoot_time_accumulator < shooting_time_interval:
		_shoot_time_accumulator += delta
	
	if state == STATE_ALIVE:
		if Input.is_action_pressed("move_left"):
			set_applied_torque(-rotation_speed)
		
		if Input.is_action_pressed("move_right"):
			set_applied_torque(rotation_speed)
			
		if not Input.is_action_pressed("move_left") and not Input.is_action_pressed("move_right"):
			set_angular_velocity(0.0)
		
		if Input.is_action_pressed("move_forward"):
			var v = Vector2(-cos(get_rotation() + deg2rad(90)),-sin(get_rotation() + deg2rad(90)))
			set_applied_force(v.normalized() * speed)
		
		if Input.is_action_just_pressed("move_forward"):
			_particles_node.emitting = true
		
		if Input.is_action_just_released("move_forward"):
			_particles_node.emitting = false
			set_applied_force(Vector2(0.0,0.0))
			
		if Input.is_action_just_pressed("shoot_common") and _shoot_time_accumulator > shooting_time_interval:
			_shoot_time_accumulator = 0.0
			match weapon_common_level:
				1:
					var projectile = _ingame_node.projectiles_pool.get_new_resource()
			
					projectile.p_owner = get_name()
					projectile.type = 1
					projectile.level = weapon_common_level
					projectile.set_global_position(_projectile_position_node.get_global_position())
					projectile.set_rotation(get_global_rotation())
					
					projectile.shoot()
				2:
					var projectiles = []
					var projectile_rotation = get_global_rotation() - deg2rad(20 * 2)
					
					for projectile_index in range(1,6):
						var projectile = _ingame_node.projectiles_pool.get_new_resource()
						
						projectile.enable_sound = false
						projectile.p_owner = get_name()
						projectile.type = 1
						projectile.level = weapon_common_level
						projectile.set_global_position(_projectile_position_node.get_global_position())
						projectile.set_rotation(projectile_rotation)
						
						projectiles.append(projectile)
						
						projectile_rotation += deg2rad(20)
					
					projectiles[0].enable_sound = true
					
					for projectile in projectiles:
						projectile.shoot()
		
		if Input.is_action_just_pressed("shoot_missile") and weapon_missile_remaining > 0  and _shoot_time_accumulator > shooting_time_interval:
			_shoot_time_accumulator = 0.0
			var projectile = _ingame_node.projectiles_pool.get_new_resource()
	
			projectile.p_owner = get_name()
			projectile.type = 2
			projectile.level = weapon_missile_level
			projectile.set_global_position(_projectile_position_node.get_global_position())
			projectile.set_rotation(get_global_rotation())
			
			projectile.shoot()
			
			weapon_missile_remaining -= 1
		
		if Input.is_action_just_pressed("shoot_laser") and weapon_laser_remaining > 0 and _shoot_time_accumulator > shooting_time_interval:
			_shoot_time_accumulator = 0.0
			var projectile = _ingame_node.projectiles_pool.get_new_resource()
	
			projectile.p_owner = get_name()
			projectile.type = 3
			projectile.level = weapon_laser_level
			projectile.set_global_position(_projectile_position_node.get_global_position())
			projectile.set_rotation(get_global_rotation())
			
			projectile.shoot()
			weapon_laser_remaining -= 1

	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = current_pos
	
	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)

# Public methods
func apply_damage(value):
	if invulnerable:
		return

	var temp_value = value
	
	if shield > 0.0:
		if shield >= temp_value:
			temp_value -= value
			shield -= value
		else:
			temp_value -= shield
			shield = 0.0
	
	if temp_value > 0.0:
		life = 0.0 if life <= temp_value else life - temp_value
	
	if life == 0.0:
		emit_signal("died")
		die()

func add_life(value):
	life = 100.0 if value >= 100.0 - life else life + value

func add_shield(value):
	shield = 100.0 if value >= 100.0 - shield else shield + value

func add_powerup(type, value):
	match type:
		PowerUp.TYPE_LIFE:
			add_life(value)
		PowerUp.TYPE_SHIELD:
			add_shield(value)
		PowerUp.TYPE_WEAPON_COMMON:
			if weapon_common_level == 1:
				weapon_common_level = 2
		PowerUp.TYPE_WEAPON_MISSILE:
			if weapon_missile_remaining > 20 and weapon_missile_level == 1:
				weapon_missile_level = 2
			weapon_missile_remaining += value
		PowerUp.TYPE_WEAPON_LASER:
			if weapon_laser_remaining > 20 and weapon_laser_level == 1:
				weapon_laser_level = 2
			weapon_laser_remaining += value

func reset():
	_particles_node.emitting = false
	_explosion_particles_node.emitting = false
	
	_collision_node.set_disabled(false)
	_sprite_node.set_visible(true)
	
	# Set ship to the center of screen
	set_linear_velocity(Vector2(0.0,0.0))
	set_angular_velocity(0.0)
	set_global_position(Engine.get_main_loop().get_root().get_size_override() / 2.0)
	set_global_rotation(deg2rad(0))
	
	invulnerable = true
	_sprite_node.set_animation("Invulnerable")
	
	state = STATE_ALIVE
	life = 100.0
	shield = 100.0
	weapon_common_level = 2
	weapon_missile_level = 2
	weapon_laser_level = 1
	weapon_missile_remaining = 10
	weapon_laser_remaining = 10
	
	_timer_invulnerability_node.start()

func die():
	_explosion_particles_node.emitting = true
	_particles_node.emitting = false
	_collision_node.set_disabled(true)
	_sprite_node.set_visible(false)
	
	state = STATE_DYING
	lifes -=  1
	
	set_linear_velocity(Vector2(0.0,0.0))
	set_angular_velocity(0.0)
	set_applied_force(Vector2(0.0,0.0))
	set_applied_torque(0.0)
	
	if lifes == 0:
		state = STATE_DIED

	_explosion_audioplayer_node.play()
	_timer_explosion_node.start()

func apply_preferences():
	# Invulnerability timer
	_timer_invulnerability_node.connect("timeout", self, "_on_invulnerability_timeout")
	_timer_invulnerability_node.set_wait_time(invulnerability_time)
	_timer_invulnerability_node.set_one_shot(true)
	
	# Explosion timer
	_timer_explosion_node.connect("timeout", self, "_on_explosion_timeout")
	_timer_explosion_node.set_wait_time(explosion_time)
	_timer_explosion_node.set_one_shot(true)
	
	# Set sprite_node
	_sprite_node.set_animation("Default")

# Private methods
func _on_explosion_timeout():
	if state == STATE_DIED:
		print("Ship was died")
	else:
		reset()

func _on_invulnerability_timeout():
	invulnerable = false
	_sprite_node.set_animation("Default")
