# Inheritance
extends Node2D

# Script instances
var SpaceShip = load("res://Scripts/Components/SpaceShip.gd")

# Enumerations
enum {TYPE_LIFE = 1, TYPE_SHIELD = 2, TYPE_WEAPON_COMMON = 3, TYPE_WEAPON_MISSILE = 4, TYPE_WEAPON_LASER = 5}

# Public variables
export(int, 1, 5) var type = 1
export(int) var value = 0 
export(float) var life_time = 10.0

onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# PoolableResource variables
var is_pool_initialized = false
var is_pool_added_to_tree_on_init = false

# Private variables
onready var _sprite_node = get_node(@"Sprite")
onready var _pickup_area_node = get_node(@"Pickup_Area")
onready var _collision_node = get_node(@"Pickup_Area/Collision")
onready var _pickup_audioplayer_node = get_node(@"Pickup_AudioPlayer")
onready var _timer_lifetime_node = get_node(@"Timer_LifeTime")

# Engine methods
func _notification(what):
	if what == NOTIFICATION_INSTANCED:
		# *****************************************************************
		# Classic / Default mode
		# *****************************************************************
		if Engine.get_main_loop().get_root().get_node("/root/GameRoot").preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Default/Sprites/PowerUps/PowerUps_Sprites.tres"))
			get_node(@"Pickup_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Pickup.ogg"))				
			
		else: # Classic Mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Classic/Sprites/PowerUps/PowerUps_Sprites.tres"))
			get_node(@"Pickup_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Pickup.ogg"))				
		# *****************************************************************
		
		get_node(@"Pickup_Area").connect("body_entered", self, "_on_body_entered")
		get_node(@"Pickup_AudioPlayer").connect("finished", self, "_on_pickup_audio_finished")
		get_node(@"Timer_LifeTime").connect("timeout", self, "_on_life_timeout")

func _process(delta):
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()
	
	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)

# Public methods
func get_type_string(type):
	return "Type" + str(type)

func apply_preferences():
	# Life timer
	_timer_lifetime_node.set_wait_time(life_time)
	_timer_lifetime_node.set_one_shot(true)
	
	# Set sprite_node
	_sprite_node.set_animation(get_type_string(type))

func reset():
	set_process(false)
	
	_sprite_node.set_visible(false)
	_collision_node.set_disabled(true)
	_timer_lifetime_node.stop()
	_pickup_audioplayer_node.stop()

func run():
	set_process(true)
	
	_sprite_node.set_visible(true)
	_collision_node.set_disabled(false)
	
	apply_preferences()
	
	_timer_lifetime_node.start()

# Private methods
func _on_life_timeout():
	self.pool_free()

func _on_body_entered(body):
	if body is SpaceShip:
		_collision_node.set_disabled(true)
		_sprite_node.set_visible(false)
		
		body.add_powerup(type, value)
		
		_pickup_audioplayer_node.play()

func _on_pickup_audio_finished():
	self.pool_free()

# PoolableResource methods
func pool_initialize():
	is_pool_initialized = true

func pool_free():
	reset()
	
	if not is_pool_added_to_tree_on_init:
		get_parent().remove_child(self)

	is_pool_initialized = false
