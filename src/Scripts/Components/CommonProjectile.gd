# Inherits
extends Node2D

# Enumerations
enum {TYPE_COMMON = 1, TYPE_MISSILE = 2, TYPE_LASER = 3}
enum {LEVEL_1 = 1, LEVEL_2 = 2}

# Public variables
export(bool) var enable_sound = true
export(String) var p_owner = ""
export(int,1,3) var type = 1
export(int,1,2) var level = 1
export(bool) var collision_only_one = false
export(float) var explosion_range = 40
export(float) var life_time = 0.5
export(float) var speed = 1000.0
export(float) var damage = 10.0
export(float) var explosion_time = 2.0

onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# PoolableResource variables
var is_pool_initialized = false
var is_pool_added_to_tree_on_init = false

# Private variables
var _can_move = false

onready var _sprite_node = get_node(@"Sprite")
onready var _collision_area_node = get_node(@"Collision_Area")
onready var _timer_lifetime_node = get_node(@"Timer_LifeTime")
onready var _shoot_common_audioplayer_node = get_node(@"Shoot_Common_AudioPlayer")
onready var _shoot_missile_audioplayer_node = get_node(@"Shoot_Missile_AudioPlayer")
onready var _shoot_laser_audioplayer_node = get_node(@"Shoot_Laser_AudioPlayer")
onready var _collision_audioplayer_node = get_node(@"Collision_AudioPlayer")
onready var _missile_particles_node = get_node(@"Missile_Particles")
onready var _explosion_area_node = get_node(@"Explosion_Area")
onready var _explosion_particles_node = get_node(@"Explosion_Particles")
onready var _explosion_audioplayer_node = get_node(@"Explosion_AudioPlayer")
onready var _explosion_collision_node = get_node(@"Explosion_Area/Collision")
onready var _timer_explosion_node = get_node(@"Timer_Explosion")
var _collision_nodes = []
var _explosion_particles_texture = null
var _missile_particles_texture = null

# Script instances
var SpaceShip = load("res://Scripts/Components/SpaceShip.gd")
var Asteroid = load("res://Scripts/Components/Asteroid.gd")
var EnemyShip = load("res://Scripts/Components/EnemyShip.gd")

# Engine methods
func _notification(what):
	if what == NOTIFICATION_INSTANCED:
		# *****************************************************************
		# Classic / Default mode
		# *****************************************************************
		if Engine.get_main_loop().get_root().get_node("/root/GameRoot").preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Default/Sprites/Projectiles/Projectiles_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Explosion.ogg"))		
			get_node(@"Collision_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Shoot_Collision.ogg"))		
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))
			get_node(@"Missile_Particles").set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))
			get_node(@"Shoot_Common_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Shoot_Normal.ogg"))
			get_node(@"Shoot_Missile_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Shoot_Missile.ogg"))
			get_node(@"Shoot_Laser_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Shoot_Laser.ogg"))
			
			_explosion_particles_texture = preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png")
			_missile_particles_texture = preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png")
		else: # Classic Mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Classic/Sprites/Projectiles/Projectiles_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Explosion.ogg"))		
			get_node(@"Collision_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Shoot_Collision.ogg"))		
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
			get_node(@"Missile_Particles").set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
			get_node(@"Shoot_Common_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Shoot_Normal.ogg"))
			get_node(@"Shoot_Missile_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Shoot_Missile.ogg"))
			get_node(@"Shoot_Laser_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Shoot_Laser.ogg"))
			
			_explosion_particles_texture = preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png")
			_missile_particles_texture = preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png")
		# ****************************************************************
		
		get_node(@"Collision_Area").connect("body_entered", self, "_on_body_entered")
		get_node(@"Timer_LifeTime").connect("timeout", self, "_on_life_timeout")
		get_node(@"Timer_Explosion").connect("timeout", self, "_on_explosion_timeout")
		
		for n_type in range(1,4):
			for n_level in range(1,3):
				_collision_nodes.append(get_node("Collision_Area/" + "Collision_" + get_type_string(n_type) + "_" + get_level_string(n_level)))
	
func _ready():
	reset()

func _process(delta):
	# Translate projectile
	if _can_move:
		var direction = Vector2(-cos(get_rotation() + deg2rad(90)),-sin(get_rotation() + deg2rad(90)))
		translate(direction.normalized() * speed * delta)
	
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()
	
	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)

# Public methods
func get_type_string(type):
	return "Type" + str(type)

func get_level_string(level):
	return "Level" + str(level)

func apply_parameters():
	# Set sprite_node
	_sprite_node.set_animation(get_type_string(type) + get_level_string(level))
	
	# Life timer	
	_timer_lifetime_node.set_wait_time(life_time)
	_timer_lifetime_node.set_one_shot(true)
	
	# Explosion timer	
	_timer_explosion_node.set_wait_time(explosion_time)
	_timer_explosion_node.set_one_shot(true)
	
	# Set collisions
	for n_type in range(1,4):
		for n_level in range(1,3):
			if type == n_type and level == n_level:
				_collision_nodes[((2 * n_type) - 2) + (n_level - 1)].set_disabled(false)
			else:
				_collision_nodes[((2 * n_type) - 2) + (n_level - 1)].set_disabled(true)
	
	# Special parameters for each type and level	
	_explosion_collision_node.set_disabled(false)
	_explosion_particles_node.emitting = false	
	
	match type:
		1: 
			damage = 5
			collision_only_one = true 
			_missile_particles_node.set_texture(null)
			_missile_particles_node.emitting = false
			explosion_range = 40.0
		2:
			collision_only_one = true
			damage = 5
			_missile_particles_node.set_texture(_missile_particles_texture)
			_missile_particles_node.emitting = true
			
			match level:
				1:
					explosion_range = 40.0
				2:
					explosion_range = 300.0
		3:
			collision_only_one = false
			damage =  10
			_missile_particles_node.set_texture(null)
			_missile_particles_node.emitting = false
			explosion_range = 40.0
	
	# Set explosion radius
	_explosion_collision_node.get_shape().set_radius(explosion_range)
	_explosion_particles_node.get_process_material().set_emission_sphere_radius(explosion_range)

func missile_explode():
	_explosion_particles_node.set_texture(_explosion_particles_texture)
	_explosion_particles_node.emitting = true
	
	for body in _explosion_area_node.get_overlapping_bodies():
		if p_owner != body.get_name() and body.has_method("apply_damage"):
			body.apply_damage(damage)

	# Hide projectile
	_can_move = false
	
	# Set collisions
	for collision_node in _collision_nodes:
		collision_node.set_disabled(true)

	_sprite_node.set_visible(false)
	
	_explosion_audioplayer_node.play()
	
	_timer_explosion_node.start()

func shoot():
	run()

func reset():
	set_process(false)
	set_process_internal(false)
	_can_move = false
	
	# Set collisions
	for collision_node in _collision_nodes:
		collision_node.set_disabled(true)
	
	_explosion_collision_node.set_disabled(true)
	
	_sprite_node.set_visible(false)
	
	_collision_area_node.set_monitoring(false)
	_collision_area_node.set_monitorable(false)
	_timer_lifetime_node.stop()
	_timer_explosion_node.stop()
	
	_shoot_common_audioplayer_node.stop()
	_shoot_missile_audioplayer_node.stop()
	_shoot_laser_audioplayer_node.stop()
	
	_explosion_particles_node.set_texture(null)
	_explosion_particles_node.emitting = false
	_missile_particles_node.set_texture(null)
	_missile_particles_node.emitting = false

func run():
	set_process(true)
	set_process_internal(true)
	
	apply_parameters()
	
	_collision_area_node.set_monitoring(true)
	_collision_area_node.set_monitorable(true)
	_sprite_node.set_visible(true)
	_timer_lifetime_node.start()
	
	_can_move = true

	if enable_sound:
		match type:
			1:
				_shoot_common_audioplayer_node.play()
			2:
				_shoot_missile_audioplayer_node.play()
			3:
				_shoot_laser_audioplayer_node.play()

# Private methods
func _on_life_timeout():
	if type == TYPE_MISSILE:
		missile_explode()
	else:
		self.pool_free()

func _on_explosion_timeout():
	self.pool_free()

func _on_body_entered(body):
	if p_owner != body.get_name():
		print("COLLISION: " + body.get_name() + " POOL_INIT: " + str(is_pool_initialized))
		if collision_only_one:
			_timer_lifetime_node.stop()
		
		if type == TYPE_MISSILE:
			missile_explode()
		else:
			if body.has_method("apply_damage"):
				body.apply_damage(damage)

			_collision_audioplayer_node.play()
				
			if collision_only_one:
				call_deferred("pool_free")
	
# PoolableResource methods
func pool_initialize():
	is_pool_initialized = true

func pool_free():
	reset()
	
	if not is_pool_added_to_tree_on_init:
		get_parent().remove_child(self)

	is_pool_initialized = false
