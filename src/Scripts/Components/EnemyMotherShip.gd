# Inheritance
extends RigidBody2D

# Enumeratons
enum {STATE_IDLE = 1, STATE_SPAWNING = 2}

# Public variables
export(int) var max_spawned_ships = 3
export(int, 1, 2) var state = 1
export(float) var life = 100.0
export(Vector2) var impulse = Vector2(0.0,0.0)
export(float) var shoot_time = 3.0
export(float) var explosion_time = 3.0
export(float) var spawns_time = 20.0
export(float) var spawning_time = 5.0
export(float) var speed = 150.0
export(float) var damage = 10.0
export(int) var score = 80

var spawned_ships = 0

onready var current_wkf_element = get_node(@"/root/GameRoot").workflow_system.current_element
onready var preferences_system = get_node(@"/root/GameRoot").preferences_system

# PoolableResource variables
var is_pool_initialized = false
var is_pool_added_to_tree_on_init = false

# Private variables
onready var _sprite_node = get_node(@"Sprite")
onready var _collision_node = get_node(@"Collision")
onready var _idle_audioplayer_node = get_node(@"Idle_AudioPlayer")
onready var _spawning_audioplayer_node = get_node(@"Spawning_AudioPlayer")
onready var _explosion_audioplayer_node = get_node(@"Explosion_AudioPlayer")
onready var _explosion_particles_node = get_node(@"Explosion_Particles")
onready var _collision_check_area_node = get_node(@"Collision_CheckArea")
onready var _collision_checker_node = get_node(@"Collision_CheckArea/Collision_Checker")
onready var _timer_shoot_node = get_node(@"Timer_Shoot")
onready var _timer_spawns_node = get_node(@"Timer_Spawns")
onready var _timer_spawning_time_node = get_node(@"Timer_SpawningTime")
onready var _timer_explosion_node = get_node(@"Timer_Explosion")
onready var _spawn_position_1_node = get_node(@"Spawn_Position_1")
onready var _spawn_position_2_node = get_node(@"Spawn_Position_2")
onready var _spawn_position_3_node = get_node(@"Spawn_Position_3")
var _ingame_node = null
var _actual_life = 100.0
var _explosion_particles_texture = null

# Signals
signal died

# Engine methods
func _notification(what):
	if what == NOTIFICATION_INSTANCED:
		# *****************************************************************
		# Classic / Default mode
		# *****************************************************************
		if Engine.get_main_loop().get_root().get_node("/root/GameRoot").preferences_system.get_parameter("Game","ClassicalMode", false) == false: # Default mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Default/Sprites/EnemyMotherShip/EnemyMotherShip_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/Explosion.ogg"))		
			get_node(@"Idle_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/EnemyMotherShip_Idle.ogg"))		
			get_node(@"Spawning_AudioPlayer").set_stream(preload("res://Assets/SFX/Default/EnemyMotherShip_Spawning.ogg"))		
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png"))		
			
			_explosion_particles_texture = preload("res://Assets/GFX/Default/Sprites/SpaceShip/Fire_Particle.png")
		else: # Classic Mode
			get_node(@"Sprite").set_sprite_frames(preload("res://Assets/GFX/Classic/Sprites/EnemyMotherShip/EnemyMotherShip_Sprites.tres"))
			get_node(@"Explosion_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/Explosion.ogg"))		
			get_node(@"Idle_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/EnemyMotherShip_Idle.ogg"))		
			get_node(@"Spawning_AudioPlayer").set_stream(preload("res://Assets/SFX/Classic/EnemyMotherShip_Spawning.ogg"))		
			get_node(@"Explosion_Particles").set_texture(preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png"))
			
			_explosion_particles_texture = preload("res://Assets/GFX/Classic/Sprites/SpaceShip/Fire_Particle.png")
		# *****************************************************************
		
		get_node(@"Timer_Shoot").connect("timeout", self, "_on_shoot_timeout")
		get_node(@"Timer_Explosion").connect("timeout", self, "_on_explosion_timeout")
		get_node(@"Timer_Spawns").connect("timeout", self, "_on_spawns_timeout")
		get_node(@"Timer_SpawningTime").connect("timeout", self, "_on_spawning_timeout")
	
func _ready():
	_ingame_node = get_node(str(current_wkf_element.scene_node.get_path()) + "/Ingame")

func _process(delta):
	# Evade objects
	if state == STATE_IDLE:
		if _collision_check_area_node.get_overlapping_bodies().size() == 0:
			set_linear_velocity(impulse.normalized() * speed)
		else:
			var result_force = Vector2(0.0,0.0) 
			
			for body in _collision_check_area_node.get_overlapping_bodies():
				result_force +=  get_global_position() - body.get_global_position()
	
			set_linear_velocity(result_force.normalized() * speed)
	else:
		set_linear_velocity(Vector2(0.0,0.0))
	
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()

	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)

# Public methods
func apply_damage(value):
	_actual_life = 0.0 if _actual_life <= value else _actual_life - value
	
	if _actual_life == 0.0:
		emit_signal("died")
		die()

func die():
	_idle_audioplayer_node.stop()
	_explosion_particles_node.set_texture(_explosion_particles_texture)
	_explosion_particles_node.emitting = true
	_timer_shoot_node.stop()
	_timer_spawns_node.stop()
	_timer_spawning_time_node.stop()
	
	set_mode(RigidBody2D.MODE_STATIC)
	_sprite_node.set_visible(false)
	_collision_node.set_disabled(true)
	
	# Add score
	_ingame_node.score += score
	
	# Spawn powerup
	_ingame_node.spawn_powerup(get_global_position(), randi() % 5 + 1, randi() % 20 + 10)
	
	_explosion_audioplayer_node.play()
	_timer_explosion_node.start()

func apply_parameters():
	_timer_shoot_node.wait_time = shoot_time
	_timer_shoot_node.one_shot = false
	
	_timer_explosion_node.wait_time = explosion_time
	_timer_explosion_node.one_shot = true
	
	_timer_spawns_node.wait_time = spawns_time
	_timer_spawns_node.one_shot = false
	
	_timer_spawning_time_node.wait_time = spawning_time
	_timer_spawning_time_node.one_shot = true	
	
	# Set sprite_node
	_sprite_node.set_animation("Default")
	
	state = STATE_IDLE

func reset():
	set_process(false)
	
	set_mode(RigidBody2D.MODE_STATIC)
	set_contact_monitor(false)
	set_sleeping(true)
	_sprite_node.set_visible(false)
	_collision_node.set_disabled(true)
	_idle_audioplayer_node.stop()
	_spawning_audioplayer_node.stop()
	_explosion_audioplayer_node.stop()
	_timer_shoot_node.stop()
	_explosion_particles_node.set_texture(null)
	_explosion_particles_node.emitting = false
	_collision_checker_node.set_disabled(true)
	_timer_spawns_node.stop()
	_timer_spawning_time_node.stop()
	_timer_explosion_node.stop()

func run():
	set_process(true)
	
	set_mode(RigidBody2D.MODE_CHARACTER)
	set_contact_monitor(true)
	set_sleeping(false)
	_sprite_node.set_visible(true)
	_collision_node.set_disabled(false)
	_collision_checker_node.set_disabled(false)
	
	apply_parameters()
	
	_timer_shoot_node.start()
	_timer_spawns_node.start()
	_idle_audioplayer_node.play()

# Private methods
func _on_shoot_timeout():
	var projectile = _ingame_node.projectiles_pool.get_new_resource()
	
	projectile.p_owner = get_name()
	projectile.type = 3
	projectile.level = 1
	projectile.set_global_position(get_global_position())	
	projectile.set_global_rotation(deg2rad(90) + (_ingame_node.get_ship_node().get_global_position() - get_global_position()).angle())
	
	projectile.shoot()

func _on_explosion_timeout():
	self.pool_free()

func _on_spawns_timeout():
	if spawned_ships == max_spawned_ships:
		return

	# Spawn enemies
	_sprite_node.set_animation("Spawning")
	_spawning_audioplayer_node.play()
	_timer_spawning_time_node.start()
	state = STATE_SPAWNING
	
	# Spawn ships
	var spawn_nodes = [_spawn_position_1_node,_spawn_position_2_node,_spawn_position_3_node]
	var impulses = [Vector2(-1.0, 1.0), Vector2(0.0, 1.0), Vector2(1.0,1.0)]
	
	for n_spawn in range(0,3):
		if spawned_ships == max_spawned_ships:
			break
		
		var enemy_ship = _ingame_node.enemy_ships_pool.get_new_resource()
		
		enemy_ship.connect("died", self, "_on_enemy_ship_died")
		enemy_ship.set_global_position(spawn_nodes[n_spawn].get_global_position())
		enemy_ship.impulse = impulses[n_spawn]
		
		enemy_ship.run()
		
		spawned_ships += 1

func _on_spawning_timeout():
	# Finish the spawn
	_sprite_node.set_animation("Default")
	_spawning_audioplayer_node.stop()
	state = STATE_IDLE

func _on_enemy_ship_died():
	spawned_ships -= 1

# PoolableResource methods
func pool_initialize():
	
	is_pool_initialized = true

func pool_free():
	reset()
	
	if not is_pool_added_to_tree_on_init:
		get_parent().remove_child(self)

	is_pool_initialized = false
