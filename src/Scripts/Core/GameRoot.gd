# Inheritance
extends Node

# Script instances
const WorkflowSystem = preload("res://Scripts/Core/WorkflowSystem.gd")
const LoggingSystem = preload("res://Scripts/Core/LoggingSystem.gd")
const PreferencesSystem = preload("res://Scripts/Core/PreferencesSystem.gd")
const RankingSystem = preload("res://Scripts/Core/RankingSystem.gd")

# Public variables
var workflow_system = WorkflowSystem.new()
var logging_system = LoggingSystem.new()
var preferences_system = PreferencesSystem.new()
var ranking_system = RankingSystem.new()