# Inheritance
extends Reference

# Script instances
var WorkflowSystem = load("res://Scripts/Core/WorkflowSystem.gd")
var InputManagerControl = load("res://Scripts/Core/InputManagerControl.gd")

# Enumerations
enum {WORKFLOW_ELEMENT_PERMAMENT = 0, WORKFLOW_ELEMENT_DYNAMIC = 1}

# Public variables
var workflow_system = null
var name = ""
var type = WORKFLOW_ELEMENT_DYNAMIC
var scene_path = ""
var show_cursor = false
var next_element_name = ""
var additive_load = false
var keep_active = true
var can_pause = true
var cursor_texture_path = ""
var parameters = {}
var scene_node = null
var loaded_scene = false
var loader = null
var loading_async = false

# Engine methods
func _get_property_list():
	return [
	{"name": "name", "type": TYPE_STRING, "hint": PROPERTY_HINT_NONE, "hint_name": "Name of the element", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "type", "type": TYPE_INT, "hint": PROPERTY_HINT_NONE, "hint_name": "Type of the element", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "scene_path", "type": TYPE_STRING, "hint": PROPERTY_HINT_FILE, "hint_name": "Path of the scene to use", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "show_cursor", "type": TYPE_BOOL, "hint": PROPERTY_HINT_NONE, "hint_name": "Use cursor?", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "next_element_name", "type": TYPE_STRING, "hint": PROPERTY_HINT_NONE, "hint_name": "Next element to load", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "additive_load", "type": TYPE_BOOL, "hint": PROPERTY_HINT_NONE, "hint_name": "Use additive loading?", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "keep_active", "type": TYPE_BOOL, "hint": PROPERTY_HINT_NONE, "hint_name": "Drops from the tree when is inactive?", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "can_pause", "type": TYPE_BOOL, "hint": PROPERTY_HINT_NONE, "hint_name": "Can pause the scene?", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "cursor_texture_path", "type": TYPE_STRING, "hint": PROPERTY_HINT_NONE, "hint_name": "Path of the cursor texture", "usage": PROPERTY_USAGE_DEFAULT},
	{"name": "parameters", "type": TYPE_DICTIONARY, "hint": PROPERTY_HINT_NONE, "hint_name": "Parameters", "usage": PROPERTY_USAGE_DEFAULT},
	]

func _get(property):
	if property == "name":
		return name
	elif property == "type":
		return type
	elif property == "scene_path":
		return scene_path
	elif property == "show_cursor":
		return show_cursor
	elif property == "next_element_name":
		return next_element_name
	elif property == "additive_load":
		return additive_load
	elif property == "keep_active":
		return keep_active
	elif property == "can_pause":
		return can_pause
	elif property == "cursor_texture_path":
		return cursor_texture_path
	elif property == "parameters":
		return parameters
	else:
		return null
	
func _set(property, value):
	if property == "name":
		name = value
	elif property == "type":
		type = value
	elif property == "scene_path":
		scene_path = value
	elif property == "show_cursor":
		show_cursor = value	
	elif property == "next_element_name":
		next_element_name = value
	elif property == "additive_load":
		additive_load = value
	elif property == "keep_active":
		keep_active = value
	elif property == "can_pause":
		can_pause = value
	elif property == "cursor_texture_path":
		cursor_texture_path = value
	elif property == "parameters":
		parameters = value

func _init(wkf_system, e_name, e_type, e_scene_path, e_show_cursor = false, e_next_element_name = "", e_additive_load = false, e_keep_active = false, e_can_pause = false, e_cursor_texture_path = ""):
	if wkf_system and wkf_system is WorkflowSystem:
		workflow_system = wkf_system

	name = e_name
	type = e_type
	scene_path = e_scene_path	
	show_cursor = e_show_cursor
	next_element_name = e_next_element_name
	additive_load = e_additive_load
	keep_active = e_keep_active
	can_pause = e_can_pause
	cursor_texture_path = e_cursor_texture_path

# Public methods
func begin():
	if show_cursor == true:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		Input.set_custom_mouse_cursor(load(cursor_texture_path if cursor_texture_path != "" else workflow_system.cursor_texture_path))
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		Input.set_custom_mouse_cursor(null)
	
	# Load the scene
	load_scene_begin_async()

func end():
	# Unload scene associated
	if loaded_scene and next_element_name != "" and workflow_system.get_element_by_name(next_element_name).additive_load == false:
		unload_scene()

	# Set current element
	workflow_system.current_element = workflow_system.get_element_by_name(next_element_name)
	
func load_scene():
	if loaded_scene:
		return

	if not additive_load:
		workflow_system.unload_all_dynamic_elements()
	
	var temp_scene_node = load(scene_path).instance()
	
	scene_node = InputManagerControl.new()
	
	scene_node.set_name(temp_scene_node.get_name())

	scene_node.add_child(temp_scene_node)

	if scene_node and scene_node is Node:
		if not can_pause:
			scene_node.set_pause_mode(Node.PAUSE_MODE_PROCESS)
		else:
			scene_node.set_pause_mode(Node.PAUSE_MODE_INHERIT)
	else:
		print("ERROR: Scene Root node (viewport) not found.")
		scene_node = null
		return

	OS.get_main_loop().get_root().get_node(workflow_system.dynamic_elements_nodepath if type == WORKFLOW_ELEMENT_DYNAMIC else workflow_system.permanent_elements_nodepath).add_child(scene_node)
	
	loaded_scene = true

func unload_scene():
	if not loaded_scene or type == WORKFLOW_ELEMENT_PERMAMENT:
		print("Can't unload a permanent element or a element that is not loaded.")
		return
	
	# Remove node from tree	
	Engine.get_main_loop().get_root().get_node(workflow_system.dynamic_elements_nodepath).remove_child(scene_node)

	if scene_node:
		scene_node.queue_free()

	loaded_scene = false

func load_scene_begin_async():
	if loaded_scene:
		return
	
	loader = ResourceLoader.load_interactive(scene_path)

	if not additive_load:
		workflow_system.unload_all_dynamic_elements()

func load_scene_end_async():
	if loaded_scene:
		return

	var scene_node_viewport = Viewport.new()
	
	var temp_scene_node = loader.get_resource().instance()
	scene_node = InputManagerControl.new()
	
	scene_node.set_name(temp_scene_node.get_name())

	scene_node.add_child(temp_scene_node)

	if scene_node and scene_node is Node:
		if not can_pause:
			scene_node.set_pause_mode(Node.PAUSE_MODE_PROCESS)
		else:
			scene_node.set_pause_mode(Node.PAUSE_MODE_INHERIT)
	else:
		print("ERROR: Scene Root node (viewport) not found.")
		scene_node = null
		loader = null
		return

	Engine.get_main_loop().get_root().get_node(workflow_system.dynamic_elements_nodepath if type == WORKFLOW_ELEMENT_DYNAMIC else workflow_system.permanent_elements_nodepath).add_child(scene_node)
	
	loaded_scene = true
	loader = null
	loading_async = false

func process_iteration():
	if loader and loading_async == false:
		if loader.poll() != OK:
			loading_async = true
			load_scene_end_async()
