# Inheritance
extends Control

# Private variables
var _disabled = false

# Engine methods
func _init():
	anchor_top = 0.0
	anchor_right = 1.0
	anchor_left = 0.0
	anchor_bottom = 1.0
	
	margin_top = 0.0
	margin_right = 0.0
	margin_left = 0.0
	margin_bottom = 0.0

# Public methods
func set_gui_input_disabled(disable):
	for node in get_children():
		if node is BaseButton:
			node.set_disabled(disable)
	
	_disabled = disable

func is_gui_input_disabled():
	return _disabled
