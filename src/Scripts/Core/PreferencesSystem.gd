# Inheritance
extends Reference

# Enumerations
enum {CONTROL_TYPE_KEYBOARD = 0, CONTROL_TYPE_JOYSTICK = 1}

# Private variables
var _config_file = null
var _config_file_path = ""

# Public methods
func init(file_path):
	_config_file = ConfigFile.new()
	
	_config_file_path = file_path

func load_preferences():
	if not _config_file:
		return FAILED
	return _config_file.load(_config_file_path)

func save_preferences():
	if not _config_file:
		return FAILED

	return _config_file.save(_config_file_path)

func set_parameter(section, name, value):
	if not _config_file:
		return FAILED
	
	_config_file.set_value(section, name, value)
	
	return OK

func get_parameter(section, name, default_value):
	if not _config_file:
		return null
	
	return _config_file.get_value(section, name, default_value)

func apply_preferences():
	# Apply localization
	TranslationServer.set_locale(get_available_languages()[get_parameter("Game", "Language", 0)]["id"])
	
	# Apply sound and music
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"), get_parameter("Game", "SFXVolume", 0.0))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), get_parameter("Game", "MusicVolume", 0.0))
	
	# Apply graphics
	if get_parameter("Game", "Fullscreen", true) == true:
		OS.set_window_size(OS.get_screen_size(OS.get_current_screen()))
	else:
		OS.set_window_size(get_available_resolutions()[get_parameter("Game", "Resolution", 0)]["resolution"])

	OS.set_window_fullscreen(get_parameter("Game", "Fullscreen", true))
	
	# Apply controls
	for action in get_available_actions():
		InputMap.erase_action(action)
		InputMap.add_action(action)
		InputMap.action_add_event(action, get_parameter("Controls_Keyboard&Mouse", action, get_action_default_input_event(action, CONTROL_TYPE_KEYBOARD)))
		InputMap.action_add_event(action, get_parameter("Controls_Joystick", action, get_action_default_input_event(action, CONTROL_TYPE_JOYSTICK)))

func get_action_default_input_event(action, control_type):
	if control_type == CONTROL_TYPE_KEYBOARD:
		var event = InputEventKey.new()
		event.pressed = true
		if action == "move_forward":
			event.scancode = KEY_UP
		elif action == "move_left":
			event.scancode = KEY_LEFT
		elif action == "move_right":
			event.scancode = KEY_RIGHT
		elif action == "shoot_common":
			event.scancode = KEY_SPACE
		elif action == "shoot_missile":
			event.scancode = KEY_ENTER
		elif action == "shoot_laser":
			event.scancode = KEY_TAB
		elif action == "cancel":
			event.scancode = KEY_ESCAPE
		else: # Pause
			event.scancode = KEY_P

		return event
	else:
		var event_axis = InputEventJoypadMotion.new()
		var event_button = InputEventJoypadButton.new()
		if action == "move_forward":
			event_axis.axis = 1
			event_axis.axis_value = -1.0
			
			return event_axis
		elif action == "move_left":
			event_axis.axis = 0
			event_axis.axis_value = -1.0
			
			return event_axis
		elif action == "move_right":
			event_axis.axis = 0
			event_axis.axis_value = 1.0
			
			return event_axis
		elif action == "shoot_common":
			event_button.button_index = 1
			event_button.pressed = true
			
			return event_button
		elif action == "shoot_missile":
			event_button.button_index = 2
			event_button.pressed = true
			
			return event_button
		elif action == "shoot_laser":
			event_button.button_index = 3
			event_button.pressed = true
			
			return event_button
		elif action == "cancel":
			event_button.button_index = 4
			event_button.pressed = true
			
			return event_button
		else: # Pause
			event_button.button_index = 5
			event_button.pressed = true
			
			return event_button

	return InputEvent.new()

func get_screen_aspect_ratio_multiplier():
	return OS.get_screen_size().x / OS.get_screen_size().y

func get_available_resolutions():
	var common_resolutions = [240, 360, 480, 720, 1080, 1440, 2160, 4320]
	var resolutions = [] # {"name": "240p", resolution: Vector2(w,h)}
	
	for res in common_resolutions:
		var width = _get_nearest_pair(res * get_screen_aspect_ratio_multiplier())
		
		if res * width >= OS.get_screen_size().x * OS.get_screen_size().y:
			break
		
		resolutions.append({"name": str(res) + "p", "resolution": Vector2(width, res)})
	
	resolutions.append({"name": "Default", "resolution": OS.get_screen_size()})
	
	return resolutions

func get_available_languages():
	return [{"name": TranslationServer.translate("Language.en"), "id": "en"}, {"name": TranslationServer.translate("Language.es"), "id": "es"}, {"name": TranslationServer.translate("Language.fr"), "id": "fr"}, {"name": TranslationServer.translate("Language.pt"), "id": "pt"}]

func get_available_difficuties():
	return [TranslationServer.translate("Difficulty.Easy"), TranslationServer.translate("Difficulty.Medium"), TranslationServer.translate("Difficulty.Hard")]

func get_available_actions():
	return ["move_forward", "move_left", "move_right", "shoot_common", "shoot_missile", "shoot_laser", "cancel", "pause"]

func get_action_name(action):
	match action:
		"move_forward": return TranslationServer.translate("Input.move_forward")
		"move_left": return TranslationServer.translate("Input.move_left")
		"move_right": return TranslationServer.translate("Input.move_right")
		"shoot_common": return TranslationServer.translate("Input.shoot_common")
		"shoot_missile": return TranslationServer.translate("Input.shoot_missile")
		"shoot_laser": return TranslationServer.translate("Input.shoot_laser")
		"cancel": return TranslationServer.translate("Input.cancel")
		"pause": return TranslationServer.translate("Input.pause")

	return ""

# Private methods
func _get_nearest_pair(number):
	if fmod(number,2.0) == 0.0:
		return number
	
	var far = number + (2 - (fmod(number,2)))
	var near = far - 2
	
	return near if abs(far - number) > abs(number - near) else far
