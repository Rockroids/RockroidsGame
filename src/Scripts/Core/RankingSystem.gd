# Inheritance
extends Reference

# Private variables
var _rankings = [] # {"date": date, "PlayerName": name, "Record": record}

# Public methods
func open(path):
	var file = File.new()
	
	_rankings.clear()
	
	file.open_encrypted_with_pass(path, File.READ, OS.get_unique_id())
	
	if not file.is_open():
		return FAILED
	
	while(not file.eof_reached()):
		var csv_line = file.get_csv_line(";")

		if csv_line.size() < 3:
			continue
		_rankings.append({"date": csv_line[0], "player_name": csv_line[1], "record": csv_line[2]})
		
	file.close()

func save(path):
	var file = File.new()
	
	file.open_encrypted_with_pass(path, File.WRITE, OS.get_unique_id())
	
	if not file.is_open():
		return FAILED
		
	for ranking in _rankings:
		file.store_line(ranking.date + ";" + ranking.player_name + ";" + str(ranking.record))
	
	file.close()

func add_new_ranking(date, player_name, record):
	_rankings.append({"date": date, "player_name": player_name, "record": str(record)})

func get_all_rankings():
	return _rankings

func get_first_n_rankings(number):
	var rankings_temp = []
	
	_rankings.sort_custom(self, "_rankings_sort_by_record_function")
	
	for i in range(number):
		if i >= _rankings.size():
			break
		
		rankings_temp.append(_rankings[i])
		 
	return rankings_temp

# Private methods
func _rankings_sort_by_record_function(r1, r2):
	return true if int(r1.record) > int(r2.record) else false
