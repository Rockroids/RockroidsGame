# Inheritance
extends Reference

# Script instances
const WorkflowElement = preload("res://Scripts/Core/WorkflowElement.gd")

# Enumerations
enum {WORKFLOW_ELEMENT_PERMAMENT = 0, WORKFLOW_ELEMENT_DYNAMIC = 1}

# Public variables
var current_element = null setget _set_current_element
var root_element = null setget _set_root_element
var dynamic_elements = []
var permanent_elements = []
var cursor_texture_path = ""
var dynamic_elements_nodepath = "" setget _set_dynamic_elements_nodepath
var permanent_elements_nodepath = "" setget _set_permanent_elements_nodepath

# Signals
signal on_element_begin_change(old_name, new_name)
signal on_element_end_change(old_name, new_name)

# Public methods
func start():
	# Initialize permanent elements
	for element in self.permanent_elements:
		if element and element is WorkflowElement:
			element.begin()
	
	# Initialize root element
	if root_element and root_element is WorkflowElement:
		self.current_element = root_element

func finish():
	Engine.get_main_loop().quit()

func _set_current_element(element):
	if element and element is WorkflowElement:
		var old_name = current_element.name if current_element else ""
		emit_signal("on_element_begin_change", old_name, element.name)
		current_element = element

		# Enable input only in current element
		#for element in dynamic_elements:
		#	if element.loaded_scene:
		#		element.scene_node.set_gui_input_disabled(element != current_element)

		current_element.begin()
		emit_signal("on_element_end_change", old_name, element.name)
	else:
		finish()

func get_element_by_name(e_name):
	for element in permanent_elements + dynamic_elements:
		if element.name == e_name:
			return element

	return null

func add_element_simple(name, type, scene_path, show_cursor = false, next_element_name = "", additive_load = false, keep_active = false, can_pause = false, cursor_texture_path = ""):
	if type == WORKFLOW_ELEMENT_PERMAMENT:
		permanent_elements.append(WorkflowElement.new(self, name, type, scene_path, show_cursor, additive_load, keep_active, can_pause, cursor_texture_path))
	else:
		dynamic_elements.append(WorkflowElement.new(self, name, type, scene_path, show_cursor, next_element_name, additive_load, keep_active, can_pause, cursor_texture_path))

func unload_all_dynamic_elements():
	for element in dynamic_elements:
		element.unload_scene()

func process_iteration():
	for element in permanent_elements + dynamic_elements:
		element.process_iteration()

# Private methods
func _set_root_element(element):
	if element and element is WorkflowElement and element in dynamic_elements:
		root_element = element

func _set_dynamic_elements_nodepath(path):
	dynamic_elements_nodepath = "/root/Root/" + path

func _set_permanent_elements_nodepath(path):
	permanent_elements_nodepath = "/root/Root/" + path