# Inheritance
extends Reference

# Private variables
var _resources = []
var _scene_path = ""
var _parent_node = @""
var _resources_number = 0
var _add_to_tree_on_init = false
var _scene_object = null
var _parent_node_object = null

# Engine methods
func _init(scene_path, parent_node, resources_number, add_to_tree_on_init = true):
	_scene_path = scene_path
	_parent_node = parent_node
	_resources_number = resources_number
	_scene_object = load(scene_path)
	_add_to_tree_on_init = add_to_tree_on_init
	_parent_node_object = Engine.get_main_loop().get_root().get_node(_parent_node)
	
	for i in range(0,resources_number):
		_resources.append(_scene_object.instance())
		_resources.back().is_pool_added_to_tree_on_init = _add_to_tree_on_init
		
		if _add_to_tree_on_init:
			_parent_node_object.add_child(_resources.back())

# Public methods
func destroy():
	for resource in _resources:
		_parent_node_object.remove_child(resource)
		resource.queue_free()

func get_new_resource():
	for resource in _resources:
		if not resource.is_pool_initialized:
			if not _add_to_tree_on_init:
				_parent_node_object.add_child(resource)

			resource.pool_initialize()
			return resource

	return null
	
func get_resources_number():
	return _resources.size()

func get_free_resources_number():
	var number = 0
	
	for resource in _resources:
		if not resource.is_pool_initialized:
			number += 1
	
	return number

func get_used_resources_number():
	var number = 0
	
	for resource in _resources:
		if resource.is_pool_initialized:
			number += 1
	
	return number