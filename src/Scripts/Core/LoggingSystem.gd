# Inheritance
extends Reference

# Enumerations
enum {LOG_TYPE_CONSOLE = 0, LOG_TYPE_FILE = 1}
enum {LOG_MESSAGE_DEBUG = 0, LOG_MESSAGE_WARNING = 1, LOG_MESSAGE_ERROR = 2}

# Private variables
var _log_file = null
var _log_type = LOG_TYPE_CONSOLE
var _initialized = false

# Public methods
func init(type, file_path=""):
	_log_type = type
	
	if _log_type == LOG_TYPE_FILE:
		_log_file = File.new()
		if _log_file.open(file_path, _log_file.WRITE) != OK:
			_log_file = null
			_initialized = false
			return FAILED
	
	_initialized = true

func close():
	if _log_file:
		_log_file.close()
		_log_file = null

func log_message(message_type, message):
	if not _initialized:
		return

	var message_text = "[" + str(OS.get_datetime().year) + "/" +  str(OS.get_datetime().month) + "/" + str(OS.get_datetime().day) + " " + str(OS.get_datetime().hour) + ":" + str(OS.get_datetime().minute) + ":" + str(OS.get_datetime().second) + "]"
	
	if message_type == LOG_MESSAGE_WARNING:
		message_text += "<WARN> "
	elif message_type == LOG_MESSAGE_DEBUG:
		message_text += "<DEBUG> "
	else:
		message_text += "<ERROR> "
	
	message_text += message
	 
	if _log_type == LOG_TYPE_FILE:
		_log_file.store_line(message_text)
	else:
		print(message_text)
